package com.marioea.jugueteriamvc.gui;

import com.marioea.jugueteriamvc.base.JuegoMesa;
import com.marioea.jugueteriamvc.base.Jugueteria;
import com.marioea.jugueteriamvc.base.Peluche;
import com.marioea.jugueteriamvc.util.Util;


import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

public class JugueteriaControlador implements ActionListener, ListSelectionListener, WindowListener {
    private Ventana vista;
    private JugueteriaModelo modelo;
    private File ultimaRutaExportada;

    /**
     *
     * @param vista
     * @param modelo
     */

    public JugueteriaControlador(Ventana vista, JugueteriaModelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            e.printStackTrace();
        }

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);

    }


    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        switch (actionCommand) {
            case "Nuevo":
                if (hayCamposVacios()) {
                    Util.mensajeError("Los siguientes campos no pueden estar vacios\n" +
                            "NombreJuguete\nMarcaJuguete\nPrecioJuguete\nNumeroLoteJuguete" +
                            vista.datoslbl.getText());
                    break;
                }

               //metodo para definir si existe el nombre o no
                // si existe sacara un mensaje de error como que ya existe ese mensaje
                if (modelo.existeNombreJuguete(vista.nombreJugueteTxt.getText())) {
                    Util.mensajeError("Ya existe un Juguete con ese nombre\n+" +
                            vista.nombreJugueteTxt.getText());
                    break;
                }
                //si pulsamos la casilla peluche damos de alta ese peluche y sino un juego de mesa

                if (vista.pelucheRadioButton.isSelected()) {
                    modelo.altaPeluche(vista.NombreJuguete.getText(), vista.marcaJugueteTxt.getText(), Double.parseDouble(vista.precioJugueteTxt.getText()) ,
                           Integer.parseInt(vista.numeroLoteJugueteTxt.getText()) , vista.fechaFabricacionDPicker.getDate(), vista.tipoMaterialTxt.getText());
                } else {
                    modelo.altaJuegoMesa(vista.NombreJuguete.getText(), vista.marcaJugueteTxt.getText(), Double.parseDouble(vista.precioJugueteTxt.getText()) ,
                            Integer.parseInt(vista.numeroLoteJugueteTxt.getText()) , vista.fechaFabricacionDPicker.getDate(),vista.estiloJuegoTxt.getText(),Integer.parseInt(vista.tamanoTxt.getText()));
                }
                limpiarCampos();
                refrescar();
                break;
            case "Importar":
                JFileChooser selectorFichero = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivo XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (org.xml.sax.SAXException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();
                }
                break;
            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivos XML", "xml");
                int opt2 = selectorFichero2.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            case "Limpiar":
                limpiarCampos();
                refrescar();

                break;
            case "Peluche":
                vista.datoslbl.setText("Tipo-Material");
                break;
            case "Juego de Mesa":
                vista.datoslbl.setText("Estilo");
                vista.datoslbl.setText("Tamaño");
                break;


        }

    }

    /**
     * En este metodo que se encarga de asegurar si hay campos vacios
     * @return
     */
//metodo para comprobar si hay campos vacios
    private boolean hayCamposVacios() {
        if (vista.nombreJugueteTxt.getText().isEmpty() ||
                vista.marcaJugueteTxt.getText().isEmpty() ||
                vista.precioJugueteTxt.getText().isEmpty() ||
                vista.numeroLoteJugueteTxt.getText().isEmpty() ||
                vista.fechaFabricacionDPicker.getText().isEmpty()) {
            return true;
        }
        return false;
    }



//metodo para limpiar los campos

    /**
     * En este metodo nos encargamos de limpiar todos los campos
     */
    private void limpiarCampos() {
        vista.nombreJugueteTxt.requestFocus();
        vista.nombreJugueteTxt.setText(null);
        vista.marcaJugueteTxt.setText(null);
        vista.precioJugueteTxt.setText(null);
        vista.numeroLoteJugueteTxt.setText(null);
        vista.fechaFabricacionDPicker.setText(null);
        vista.tipoMaterialTxt.setText(null);
        vista.estiloJuegoTxt.setText(null);
        vista.tamanoTxt.setText(null);

    }

    /**
     * En este metodo nos encargamos de limpiar toda la lista creada en la clase Ventana
     * y para cada juguete que tengamos lo añadiremos  a la lista
     */
    private void refrescar() {
        vista.dlmJugueteria.clear();
        for (Jugueteria unJuguete : modelo.obtenerJugueteria()) {


            vista.dlmJugueteria.addElement(unJuguete);
        }
    }

    /**
     * Metodo para conseguir que todos los botones tengan eventos y asi funcionen
     * @param listener
     */
//listener para conseguir que funcionen asi los botones
    private void addActionListener(ActionListener listener) {
        vista.pelucheRadioButton.addActionListener(listener);
        vista.juegoDeMesaRadioButton.addActionListener(listener);
        vista.exportarBtn.addActionListener(listener);
        vista.importarBtn.addActionListener(listener);
        vista.nuevoBtn.addActionListener(listener);
        vista.limpiarBtn.addActionListener(listener);
    }

    /**
     * metodo para añadir una ventana
     * @param listener
     */
    private void addWindowListener(WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    /**
     * metodo para añadir una lista de seleccion
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener) {
        vista.list1.addListSelectionListener(listener);
    }

    /**
     * metodo en el cual creamos la configuracion mediante el properties
     * y cargamos el fichero de jugueteria.conf
     * y mediante la ultima ruta exportada creamos un nuevo fichero
     * que obtiene la propiedad de la ultima ruta exportada
     * @throws IOException
     */
    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("jugueteria.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    /**
     * metodo en el cual unicamente actualizamos los datos de la configuracion
     * @param ultimaRutaExportada
     */
    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    /**
     * Instanciamos las properties y mostramos la ultima ruta y el camino absoluto de esa ruta
     * luego almacenamos creando un print writer el cual nos pintara en el fichero jugueteria.conf
     * @throws IOException
     */
    private void guardarConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("jugueteria.conf"), "Datos configuracion Juguetes");

    }

    /**
     *sirve para comprobar si los datos estan cambiados
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            Jugueteria jugueteSeleccionado = (Jugueteria) vista.list1.getSelectedValue();
            vista.nombreJugueteTxt.setText((jugueteSeleccionado.getNombreJuguete()));
            vista.marcaJugueteTxt.setText(jugueteSeleccionado.getMarcaJuguete());
            vista.precioJugueteTxt.setText(String.valueOf((double) jugueteSeleccionado.getPrecioJuguete()));
            vista.numeroLoteJugueteTxt.setText(String.valueOf((int) jugueteSeleccionado.getNumeroLoteFabricacion()));
            vista.fechaFabricacionDPicker.setDate(jugueteSeleccionado.getFechaFabricacion());

            if (jugueteSeleccionado instanceof Peluche) {
                vista.pelucheRadioButton.doClick();
                vista.tipoMaterialTxt.setText(String.valueOf(((Peluche) jugueteSeleccionado).getTipoMaterial()));
            } else {
                vista.juegoDeMesaRadioButton.doClick();
                vista.tamanoTxt.setText(String.valueOf(((JuegoMesa) jugueteSeleccionado).getTamano()));
                vista.estiloJuegoTxt.setText(String.valueOf(((JuegoMesa) jugueteSeleccionado).getEstilo()));
            }
        }
    }

    /**
     * Metodo para salir de la ventana
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {
        int resp = Util.mensajeConfirmacion("¿Desea salir de la ventana?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            try {
                guardarConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }

    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

}
