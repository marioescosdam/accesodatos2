package com.marioea.jugueteriamvc.gui;

import com.marioea.jugueteriamvc.base.JuegoMesa;
import com.marioea.jugueteriamvc.base.Jugueteria;
import com.marioea.jugueteriamvc.base.Peluche;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class JugueteriaModelo {
    //creamos un array de la clase Juguteria
    private ArrayList<Jugueteria> listaJugueteria;


    //creamos constructor

    public JugueteriaModelo() {
        listaJugueteria = new ArrayList<Jugueteria>();
    }
    //creamos un metodo a raiz del array para obtener todos los Juguetes

    /**
     * metodo para obtener una jugueteria del array
     * @return
     */
    public ArrayList<Jugueteria> obtenerJugueteria() {
        return listaJugueteria;
    }

    /**
     *
     * @param nombreJuguete
     * @param marcaJuguete
     * @param precioJuguete
     * @param numeroLoteFabricacion
     * @param fechaFabricacion
     * @param tipoMaterial
     */
    //creamos un metodo para dar de alta un peluche

    /**
     * En este metodo damos de alta un peluche pasamos todas las variables
     *  de Jugueteria y las de peluche ya que hemos creado una herencia
     */
    public void altaPeluche(String nombreJuguete,String marcaJuguete, double precioJuguete, int numeroLoteFabricacion, LocalDate fechaFabricacion,String tipoMaterial){
        Peluche nuevoPeluche=new Peluche(nombreJuguete,marcaJuguete,precioJuguete,numeroLoteFabricacion,fechaFabricacion,tipoMaterial);
        listaJugueteria.add(nuevoPeluche);
    }

    /**
     *
     * En este metodo damos de alta un juego de mesa pasamos todas las variables
     *      *  de Jugueteria y las de peluche ya que hemos creado una herencia
     */
    //creamos un metodo para dar de alta un Juego de mesa
    public void altaJuegoMesa(String nombreJuguete,String marcaJuguete, double precioJuguete, int numeroLoteFabricacion, LocalDate fechaFabricacion, String estilo, int tamano){
        JuegoMesa nuevoJuegoMesa=new JuegoMesa(nombreJuguete,marcaJuguete,precioJuguete,numeroLoteFabricacion,fechaFabricacion,estilo,tamano);
        listaJugueteria.add(nuevoJuegoMesa);
    }

    /**
     * En este metodo comprobamos si existe un nombre de un juguete  retornando un true y si no existe un false
     * @param nombreJuguete
     * @return
     */
    //metodo para comprobar si existe el nombre del juguete
    public boolean existeNombreJuguete(String nombreJuguete) {
        for (Jugueteria unJuguete:listaJugueteria) {
            if(unJuguete.getNombreJuguete().equals(nombreJuguete)) {
                return true;
            }
        }
        return false;
    }

    /**
     * En este metodo nos encargamos de exportar un fichero creado en formato .xml
     * @param fichero
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    //realizamos la exportacion de todos los datos XML rellenados anteriormente
    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        //Añado el nodo raiz - la primera etiqueta que contiene a las demas es decir la etiqueta padre
        Element raiz = documento.createElement("Jugueteria");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoJugueteria = null, nodoDatos = null;
        Text texto = null;

        for (Jugueteria unJuguete : listaJugueteria) {

            /*Añado dentro de la etiqueta raiz (Jugueteria) una etiqueta
            dependiendo del tipo de vehiculo que este almacenando
            (peluche o juego de mesa)
             */
            if (unJuguete instanceof Peluche) {
                nodoJugueteria = documento.createElement("Peluche");

            } else {
                nodoJugueteria = documento.createElement("Juego de Mesa");
            }
            //inserta un nuevo nodo dentro de la estructura DOM de un documento
            raiz.appendChild(nodoJugueteria);

            /*Dentro de la etiqueta juegueteria le añado
            las subetiquetas con los datos de sus
            atributos (nombreJuguete,marcaJuguete , etc)
             */
            nodoDatos = documento.createElement("Nombre-del-Juguete");
            nodoJugueteria.appendChild(nodoDatos);
            //crea un nodo cogiendo el nombre del juguete
            texto = documento.createTextNode(unJuguete.getNombreJuguete());
            //inserta un nuevo nodo dentro de la estructura DOM de un documento
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("Marca-del-Juguete");
            nodoJugueteria.appendChild(nodoDatos);

            texto = documento.createTextNode(unJuguete.getMarcaJuguete());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("Precio-del-Juguete");
            nodoJugueteria.appendChild(nodoDatos);
            //revisar
            texto = documento.createTextNode(String.valueOf((double)unJuguete.getPrecioJuguete()));
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("Numero-de-Lote-del-Juguete");
            nodoJugueteria.appendChild(nodoDatos);
            //revisar
            texto = documento.createTextNode(String.valueOf((int)unJuguete.getNumeroLoteFabricacion()));
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("fecha-fabricacion");
            nodoJugueteria.appendChild(nodoDatos);

            texto = documento.createTextNode(unJuguete.getFechaFabricacion().toString());
            nodoDatos.appendChild(texto);

            /* Como hay un dato que depende del tipo de jugueteria
            debo acceder a él controlando el tipo de objeto
             */
            if (unJuguete instanceof Peluche) {
                nodoDatos = documento.createElement("tipo-material");
                nodoJugueteria.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Peluche) unJuguete).getTipoMaterial()));
                nodoDatos.appendChild(texto);
            } else {
                nodoDatos = documento.createElement("estilo");
                nodoJugueteria.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((JuegoMesa) unJuguete).getEstilo()));
                nodoDatos.appendChild(texto);
                nodoDatos = documento.createElement("tamano");
                nodoJugueteria.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((JuegoMesa) unJuguete).getTamano()));
                nodoDatos.appendChild(texto);

            }

        }
        /*
        Guardo los datos en "fichero" que es el objeto File
        recibido por parametro
         */
        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);
    }

    /**
     *  En este metodo nos encargamos de importar un fichero creado en formato .xml
     *  anteriormente y el cual tenemos guardado en nuestro pc
     * @param fichero
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaJugueteria = new ArrayList<Jugueteria>();
        Peluche nuevoPeluche = null;
        JuegoMesa nuevoJuegoMesa = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoJugueteria = (Element) listaElementos.item(i);


            if (nodoJugueteria.getTagName().equals("Peluche")) {
                nuevoPeluche = new Peluche();
                nuevoPeluche.setNombreJuguete(nodoJugueteria.getChildNodes().item(0).getTextContent());
                nuevoPeluche.setMarcaJuguete(nodoJugueteria.getChildNodes().item(1).getTextContent());
                nuevoPeluche.setPrecioJuguete(Double.parseDouble(nodoJugueteria.getChildNodes().item(2).getTextContent()));
                nuevoPeluche.setNumeroLoteFabricacion(Integer.parseInt(nodoJugueteria.getChildNodes().item(3).getTextContent()));
                nuevoPeluche.setFechaFabricacion(LocalDate.parse(nodoJugueteria.getChildNodes().item(4).getTextContent()));
                nuevoPeluche.setTipoMaterial(nodoJugueteria.getChildNodes().item(5).getTextContent());

                listaJugueteria.add(nuevoPeluche);
            } else {
                if (nodoJugueteria.getTagName().equals("JuegoMesa")) {
                    nuevoJuegoMesa = new JuegoMesa();
                    nuevoJuegoMesa.setNombreJuguete(nodoJugueteria.getChildNodes().item(0).getTextContent());
                    nuevoJuegoMesa.setMarcaJuguete(nodoJugueteria.getChildNodes().item(1).getTextContent());
                    nuevoJuegoMesa.setPrecioJuguete(Double.parseDouble(nodoJugueteria.getChildNodes().item(2).getTextContent()));
                    nuevoPeluche.setNumeroLoteFabricacion(Integer.parseInt(nodoJugueteria.getChildNodes().item(3).getTextContent()));
                    nuevoJuegoMesa.setFechaFabricacion(LocalDate.parse(nodoJugueteria.getChildNodes().item(4).getTextContent()));
                    nuevoJuegoMesa.setEstilo(nodoJugueteria.getChildNodes().item(5).getTextContent());
                    nuevoJuegoMesa.setTamano(Integer.parseInt(nodoJugueteria.getChildNodes().item(6).getTextContent()));


                    listaJugueteria.add(nuevoJuegoMesa);
                }
            }


        }
    }
}
