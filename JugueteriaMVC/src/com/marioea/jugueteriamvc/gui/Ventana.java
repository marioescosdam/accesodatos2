package com.marioea.jugueteriamvc.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.marioea.jugueteriamvc.base.Jugueteria;

import javax.swing.*;

public class Ventana {
    public JPanel panel1;
    public JFrame frame;
    public JRadioButton pelucheRadioButton;
    public JRadioButton juegoDeMesaRadioButton;
    public JLabel NombreJuguete;
    public JTextField nombreJugueteTxt;
    public JLabel MarcaJuguete;
    public JTextField marcaJugueteTxt;
    public JLabel PrecioJuguete;
    public JTextField precioJugueteTxt;
    public JTextField numeroLoteJugueteTxt;
    public JLabel NumeroLoteJuguete;
    public JLabel FechaFabricacion;
    public DatePicker fechaFabricacionDPicker;
    public JLabel EstiloJuguete;
    public JLabel Tamano;
    public JLabel TipoMaterial;
    public JTextField estiloJuegoTxt;
    public JTextField tamanoTxt;
    public JTextField tipoMaterialTxt;
    public JButton nuevoBtn;
    public JButton exportarBtn;
    public JButton importarBtn;
    public JList list1;
    public JLabel datoslbl;
    public JButton limpiarBtn;

    //Elementos creados por mi
    public DefaultListModel<Jugueteria> dlmJugueteria;

    public Ventana() {
        frame = new JFrame("Jugueteria MVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    private void initComponents() {
        dlmJugueteria=new DefaultListModel<Jugueteria>();
        list1.setModel(dlmJugueteria);
    }
}
