package com.marioea.jugueteriamvc.base;

import java.time.LocalDate;

public abstract class Jugueteria {
    //creamos las variables
    private String nombreJuguete;
    private String marcaJuguete;
    private double precioJuguete;
    private int numeroLoteFabricacion;
    private LocalDate fechaFabricacion;

    /**
     *
     * @param nombreJuguete
     * @param marcaJuguete
     * @param precioJuguete
     * @param numeroLoteFabricacion
     * @param fechaFabricacion
     */
//generamos el constructor
    public Jugueteria(String nombreJuguete, String marcaJuguete, double precioJuguete, int numeroLoteFabricacion, LocalDate fechaFabricacion) {
        this.nombreJuguete = nombreJuguete;
        this.marcaJuguete = marcaJuguete;
        this.precioJuguete = precioJuguete;
        this.numeroLoteFabricacion = numeroLoteFabricacion;
        this.fechaFabricacion = fechaFabricacion;
    }
    public Jugueteria() {

    }
//generamos los getters y setters

    /**
     *
     * @return creamos el get para coger el nombre del juguete
     */
    public String getNombreJuguete() {
        return nombreJuguete;
    }

    /**
     *
     * @param nombreJuguete creamos el set para mostrar ese nombre de juguete
     */
    public void setNombreJuguete(String nombreJuguete) {
        this.nombreJuguete = nombreJuguete;
    }

    /**
     *
     * @return creamos el get para obtener la marca del juguete
     */
    public String getMarcaJuguete() {
        return marcaJuguete;
    }

    /**
     *
     * @param marcaJuguete creamos el set para mostrar la marca del juguete
     */
    public void setMarcaJuguete(String marcaJuguete) {
        this.marcaJuguete = marcaJuguete;
    }

    /**
     *
     * @return creamos el get para obtener el precio del juguete
     */
    public double getPrecioJuguete() {
        return precioJuguete;
    }

    /**
     *
     * @param precioJuguete creamos el set para mostrar el precio del juguete
     */
    public void setPrecioJuguete(double precioJuguete) {
        this.precioJuguete = precioJuguete;
    }

    /**
     *
     * @return creamos el get para obtener el numero de lote de fabricacion
     */
    public int getNumeroLoteFabricacion() {
        return numeroLoteFabricacion;
    }

    /**
     *
     * @param numeroLoteFabricacion creamos el set para mostrar el numero de lote de fabricacion
     */
    public void setNumeroLoteFabricacion(int numeroLoteFabricacion) {
        this.numeroLoteFabricacion = numeroLoteFabricacion;
    }

    /**
     *
     * @return creamos el get para obtener la fecha de fabricacion la cual hemos metido como un LocalDate
     */
    public LocalDate getFechaFabricacion() {
        return fechaFabricacion;
    }

    /**
     *
     * @param fechaFabricacion creamos el set para mostrar la fecha de fabricacion
     */
    public void setFechaFabricacion(LocalDate fechaFabricacion) {
        this.fechaFabricacion = fechaFabricacion;
    }
}
