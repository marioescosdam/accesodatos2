package com.marioea.jugueteriamvc.base;

import java.time.LocalDate;

public class JuegoMesa extends Jugueteria{
    private String estilo;
    private int  tamano;

    /**
     *
     * @param nombreJuguete
     * @param marcaJuguete
     * @param precioJuguete
     * @param numeroLoteFabricacion
     * @param fechaFabricacion
     * @param estilo
     * @param tamano
     */
    //creamos constructor


    public JuegoMesa(String nombreJuguete, String marcaJuguete, double precioJuguete, int numeroLoteFabricacion, LocalDate fechaFabricacion, String estilo, int tamano) {
        super(nombreJuguete, marcaJuguete, precioJuguete, numeroLoteFabricacion, fechaFabricacion);
        this.estilo = estilo;
        this.tamano = tamano;
    }
    public JuegoMesa() {
        super();

    }
    //creamos getters y setters

    /**
     *
     * @return getter para obtener el estilo
     */
    public String getEstilo() {
        return estilo;
    }

    /**
     *
     * @param estilo setter para mostrar el estilo
     */
    public void setEstilo(String estilo) {
        this.estilo = estilo;
    }

    /**
     *
     * @return getter para obtener el tamaño
     */
    public int getTamano() {
        return tamano;
    }

    /**
     *
     * @param tamano setter para mostrar el tamaño
     */
    public void setTamano(int tamano) {
        this.tamano = tamano;
    }

    /**
     *
     * @return generamos el toString
     */
    //creamos el to String
    @Override
    public String toString() {
        return "Juego de Mesa: " + getNombreJuguete()+" "+getMarcaJuguete()+
                " "+getPrecioJuguete()+" "+getNumeroLoteFabricacion()+" "+getEstilo()+" "+getTamano();

    }
}
