package com.marioea.jugueteriamvc.base;

import java.time.LocalDate;

public class Peluche extends Jugueteria{
    //creamos las variables
    private String tipoMaterial;

    /**
     *
     * @param nombreJuguete
     * @param marcaJuguete
     * @param precioJuguete
     * @param numeroLoteFabricacion
     * @param fechaFabricacion
     * @param tipoMaterial
     */
    //creamos los constructores


    public Peluche(String nombreJuguete, String marcaJuguete, double precioJuguete, int numeroLoteFabricacion, LocalDate fechaFabricacion, String tipoMaterial) {
        super(nombreJuguete, marcaJuguete, precioJuguete, numeroLoteFabricacion, fechaFabricacion);
        this.tipoMaterial = tipoMaterial;
    }

    public Peluche() {
        super();
    }
    //creamos los getters y setters

    /**
     *
     * @return creamos el get para obtener el tipo de material
     */
    public String getTipoMaterial() {
        return tipoMaterial;
    }

    /**
     *
     * @param tipoMaterial creamos el set para mostrar el tipo de material
     */
    public void setTipoMaterial(String tipoMaterial) {
        this.tipoMaterial = tipoMaterial;
    }
    //creamos el toString

    /**
     *
     * @return generamos el toString
     */
    @Override
    public String toString() {
        return "Peluche: " + getNombreJuguete()+" "+getMarcaJuguete()+
                " "+getPrecioJuguete()+" "+getNumeroLoteFabricacion()+" "+getTipoMaterial();

    }
}
