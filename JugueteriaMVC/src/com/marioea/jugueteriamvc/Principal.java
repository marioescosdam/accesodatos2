package com.marioea.jugueteriamvc;



import com.marioea.jugueteriamvc.gui.JugueteriaControlador;
import com.marioea.jugueteriamvc.gui.JugueteriaModelo;
import com.marioea.jugueteriamvc.gui.Ventana;

public class Principal {
    public static void main(String[] args) {
        Ventana vista = new Ventana();
        JugueteriaModelo modelo = new JugueteriaModelo();
        JugueteriaControlador controlador = new JugueteriaControlador(vista, modelo);
    }
}
