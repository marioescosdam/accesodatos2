package com.marioea.jugueteriamvc.util;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

public class Util {
    /**
     * muestra un mensaje de error cuando cometes un fallo
     * @param mensaje
     */
    //te muestro un mensaje de error cuando cometes un fallo
    public static void mensajeError(String mensaje) {
        JOptionPane.showMessageDialog(null,mensaje,"Error",JOptionPane.ERROR_MESSAGE);
    }

    /**
     * muestra un mensaje de si deseas cerrar la ventana
     * @param mensaje
     * @param titulo
     * @return
     */
    //mensaje de si deseas cerrar la ventana
    public static int mensajeConfirmacion(String mensaje, String titulo) {
        return JOptionPane.showConfirmDialog(null,mensaje,titulo,JOptionPane.YES_NO_OPTION);
    }

    /**
     * metodo para crear cuadros de dialogo
     * @param rutaDefecto
     * @param tipoArchivos
     * @param extension
     * @return
     */

    public static JFileChooser crearSelectorFicheros(File rutaDefecto, String tipoArchivos, String extension) {
        JFileChooser selectorFichero = new JFileChooser();
        if (rutaDefecto!=null) {
            selectorFichero.setCurrentDirectory(rutaDefecto);
        }
        if (extension!=null) {
            FileNameExtensionFilter filtro = new FileNameExtensionFilter(tipoArchivos,extension);
            selectorFichero.setFileFilter(filtro);
        }
        return selectorFichero;
    }
}
